#!/usr/bin/env python3

from __future__ import print_function

import datetime
# https://docs.python.org/3.5/library/struct.html
import struct
import sys
import time

# https://pyserial.readthedocs.io/en/latest/shortintro.html
import serial

debug = False
port = serial.Serial('/dev/ttyUSB0', 9600)

def single_reading(port):
    """From the serial port,
    read a single 16-bit value.
    """

    # http://co2meters.com/Documentation/AppNotes/AN112-K30ProbeWebEnabled.pdf
    port.write(b'\xFE\x04\x00\x03\x00\x01\xD5\xC5')
    bytes_from_sensor = port.read(7)
    if debug:
        print("raw", bytes_from_sensor, file=sys.stderr)
    # Hints from App Note (above), suggest:
    v, = struct.unpack('>H', bytes_from_sensor[3:5])
    return v


def main():
    while 1:
        now = datetime.datetime.utcnow()
        print(datetime.datetime.isoformat(now), single_reading(port), sep=',', flush=True)
        time.sleep(2)


if __name__ == "__main__":
    main()
