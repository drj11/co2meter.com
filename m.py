# coding=utf-8
"""
Data Model for CO₂ data
"""

# co2.csv
# The input file is of the form:
#
"""
2019-05-30T10:36:41.721099,482
2019-05-30T10:36:43.752226,484
2019-05-30T10:36:45.782865,486
2019-05-30T10:36:47.813809,488
"""


import bisect
import csv
import fileinput
import glob
import re

all_csv_files = fileinput.input(glob.glob("*.csv"))
rows = sorted(csv.reader(all_csv_files))
rows = [(d, int(v)) for d, v in rows]

def Day(day):
    """
    Return all the rows for the given day.
    `day` should be a string of the form YYYY-MM-DD.
    """

    y, m, d = map(int, re.findall(r"\d+", day))

    i = bisect.bisect_left(rows, (day, 0))
    # This may be a non-valid ISO 8601 day, but
    # for searching purposes, that won't matter.
    after = "{:04d}-{:02d}-{:02d}".format(y, m, d+1)
    j = bisect.bisect_left(rows, (after, 0))
    return rows[i:j]


def All():
    """
    Return all the rows.
    """

    return rows[:]


def first_day():
    """Return the first day for which data exists.
    A string of the form YYYY-MM-DD.
    """

    return rows[0][0][:10]


def last_day():
    """Return the last day for which data exists.
    A string of the form YYYY-MM-DD.
    """
    return rows[-1][0][:10]

