#!/usr/bin/env python

# A web server that serves SVG plots of the
# time series data from the file co2.csv
#
# co2.csv
# The input file is of the form:
#
"""
2019-05-30T10:36:41.721099,482
2019-05-30T10:36:43.752226,484
2019-05-30T10:36:45.782865,486
2019-05-30T10:36:47.813809,488
"""
#
# Requires Python 3, matplotlib, pandas and calmap.  Run with:
# python3 web.py

import datetime
import http.server
import itertools
import io
import re

# https://docs.python.org/3/library/urllib.parse.html
import urllib.parse

# https://matplotlib.org/
import matplotlib.figure
import matplotlib.style

# https://pandas.pydata.org/
import pandas

# https://pypi.org/project/calmap/
import calmap

# Local. The data model.
import m

# Suggested by Will Furnass, 2019-05-31
matplotlib.style.use("ggplot")


# Parameters for sizing the plot
Y_MIN = 380
Y_MAX = 2000
X_SIZE = 11
Y_SIZE = 7


class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        split = urllib.parse.urlsplit(self.path)
        path = split.path
        self.query = split.query

        if path == "/hello":
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(b"Hello, world!\n")
            return
        if path.endswith("/co2.svg"):
            return self.view_svg()
        if path.endswith("/heat.svg"):
            return self.view_heat()
        if path.endswith("/cal.svg"):
            return self.view_cal()
        if path == "/all":
            return self.view_all()
        return self.view_dash()

    def view_svg(self):
        self.send_response(200)
        self.send_header("Content-type", "image/svg+xml")
        self.end_headers()

        match = re.search(r"\d{4}-\d{2}-\d{2}", self.path)
        if match:
            # URL has a component of the form YYYY-MM-DD
            day = match.group(0)
        else:
            day = m.last_day()

        rows = m.Day(day)

        fig = matplotlib.figure.Figure()
        fig.suptitle("CO₂ "+day)
        ax = fig.add_subplot(1, 1, 1)

        xs, ys = zip(*rows)
        xs = [datetime.datetime.fromisoformat(x) for x in xs]

        ax.set_ylim(Y_MIN, max(Y_MAX, ax.get_ylim()[1]))
        ax.plot(xs, ys)
        fig.autofmt_xdate()
        ax.set_ylabel("ppm")
        ax.set_xlabel("Date and Time")

        buffer = io.BytesIO()
        fig.savefig(buffer, format="svg")

        self.wfile.write(buffer.getbuffer())

    def view_cal(self):
        self.send_response(200)
        self.send_header("Content-type", "image/svg+xml")
        self.end_headers()

        rows = m.All()
        cells = []
        for h, vs in itertools.groupby(rows, by_day):
            cells.append((h, average(v for t,v in vs)))

        xs, ys = zip(*cells)
        xs = pandas.to_datetime(xs)
        events = pandas.Series(ys, index=xs)

        fig = matplotlib.figure.Figure(figsize=(17,4))
        fig.suptitle("Average CO₂ per day (ppm)")
        ax = fig.add_subplot(1,1,1)

        # only currently supports 2019!
        clmp = calmap.yearplot(events, year=2019, ax=ax, cmap='YlOrRd')
        fig.colorbar(clmp.get_children()[1], ax=clmp, orientation='horizontal')

        buffer = io.BytesIO()
        fig.savefig(buffer, format="svg")
        self.wfile.write(buffer.getbuffer())

    def view_heat(self):
        self.send_response(200)
        self.send_header("Content-type", "image/svg+xml")
        self.end_headers()

        qdict = urllib.parse.parse_qs(self.query)
        cmap = qdict.get('cmap', ["inferno"])[0]

        rows = m.All()
        cells = []
        for h, vs in itertools.groupby(rows, by_ten):
            cells.append((h, average(v for t,v in vs)))

        # Convert to matrix, with one row per day
        z = []
        for d, pairs in itertools.groupby(cells, by_day):
            l = list(p[1] for p in pairs)
            z.append(l)

        # Only keep a day if it has a full row of data
        z = [row for row in z if len(row) == 24*6]

        fig = matplotlib.figure.Figure()
        fig.suptitle("CO₂ all days")
        ax = fig.add_subplot(1, 1, 1)

        ax.imshow(z, cmap=cmap)
        ax.set_ylabel("day")
        ax.set_xlabel("time")
        ax.set_xticks(range(0, 150, 6))
        labels = [(str(x//6) if x%6==0 else "") for x in range(0, 150, 6)]
        ax.set_xticklabels(labels)
        ax.grid(None)

        buffer = io.BytesIO()
        fig.savefig(buffer, format="svg")

        self.wfile.write(buffer.getbuffer())

    def view_all(self):
        """
        Display all the SVGs in one page.
        """
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(
            """
<html>
<meta charset="utf-8" />
<title>CO₂ level</title>
""".encode("utf-8"))

        d = m.first_day()
        while d <= m.last_day():
            self.wfile.write("""<img src="{}/co2.svg"
            />\n""".format(d).encode("utf-8"))
            # Advance to next day.
            dt = datetime.datetime.fromisoformat(d + "T12")
            d = (dt + datetime.timedelta(days=1)).isoformat()[:10]

        self.wfile.write("""
</html>
""".encode(
                "utf-8"
            )
        )
        return

    def view_dash(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(
            """
<html>
<meta charset="utf-8" http-equiv="refresh" content="55" />
<title>CO₂ level</title>
<p>Code available on:
<a href="https://gitlab.com/drj11/co2meter.com/">https://gitlab.com/drj11/co2meter.com/</a></p>
<img src="co2.svg" />
<h2>Bibliography</h2>
<li><a
href="https://ehp.niehs.nih.gov/doi/10.1289/ehp.1510037">Associations
of Cognitive Function Scores with Carbon Dioxide, Ventilation,
and Volatile Organic Compound Exposures in Office
Workers</a></li>
</html>
""".encode(
                "utf-8"
            )
        )
        return


def by_hour(pair):
    """
    Given a tuple whose first item is
    an ISO 8601 date string of the form YYYY-MM-DDTHH:MM:SS...
    return the initial prefix that corresponds to the hour.
    """
    return pair[0][:13]

def by_ten(pair):
    """Every ten minutes."""
    return pair[0][:15]


def by_day(pair):
    return pair[0][:10]


def average(seq):
    seq = list(seq)
    return sum(seq) / len(seq)


address = ("localhost", 8550)

print("About to listen on: http://{}:{}/".format(*address))
server = http.server.HTTPServer(address, Handler)
server.serve_forever()
